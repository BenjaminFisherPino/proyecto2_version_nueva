public class Main {

	public static void main(String[] args) {
		Enfermedad e = new Enfermedad(0.2);
		Comunidad com = new Comunidad(100,10,10,0.25,e);
		int pasos = 10;
		
		for (int i = 0; i < pasos;i++) {
			com.vacunar();
			com.verificador();
			com.infectar();
			com.recuperar();
			
			com.cuentaPersonas();
		}
	
		//PARA VERIFICAR DATOS
		//com.imprime();
	}

}
