import java.util.Random;

public class Persona {

	//ATRIBUTOS
	//private Vacuna vac;
	private Double formula, valor_enf, valor_afe;
	private int id, cont, edad;
	private String enfermedad, afeccion, estado;
	private Boolean virus,inoculado;
	private Vacuna vac;
	
	Random rd = new Random();
	
	//CONSTRUCTOR
	Persona(int id, int edad){
		this.id = id;
		this.cont = 0;
		this.estado = "Sano";
		this.edad = edad;
		this.virus = false;
		this.inoculado = false;
	}
	
	//METODOS
	public int getId() {
		return this.id;
	}
	
	public void setEstado(String a) {
		this.estado = a;
	}
	
	//Setear la vacuna
	public void setVacuna(Vacuna a) {
		this.vac = a;
		this.inoculado = true;
	}
	
	public Vacuna getVacuna() {
		return this.vac;
	}
	
	public Boolean getInoculado() {
		return this.inoculado;
	}
	
	//Para saber si esta enfermo o no
	public void setVirus(Boolean a) {
		this.virus = a;
		if(this.virus) {
			this.estado = "Enfermo";
		}
		
		else {
			this.estado = "recuperado";
		}
	}
	
	public Boolean getVirus() {
		return this.virus;
	}
	
	//Nos setea las enfermedades de la persona
	public void setEnfermedades() {
		int temp = rd.nextInt(100)+1;
		if(temp <= 25) {
			String enfermedades[] = new String[]{"Asma", "CerVas", "Fibrosis", "Hipertension", "Presion arterial"};
			this.enfermedad = enfermedades[new Random().nextInt(enfermedades.length)];
			
			switch(this.enfermedad) {
			
			case ("Asma"):
				this.valor_enf = 0.1;
				break;
			
			case ("CerVas"):
				this.valor_enf = 0.1;
				break;
			
			case ("Fibrosis"):
				this.valor_enf = 0.1;
				break;
			
			case ("Hipertension"):
				this.valor_enf = 0.1;
				break;
			
			case ("Presion arterial"):
				this.valor_enf = 0.1;
				break;
			}										
		}
		
		else {
			this.enfermedad = "Sin enfermedad";
			this.valor_enf = 0.0;
		}
	}
	
	public String getEnfermedad() {
		return this.enfermedad;
	}
	
	//Nos setea las afecciones de la persona
	public void setAfeccion() {
		int temp = rd.nextInt(100)+1;
		if(temp <= 65) {
			String afecciones[] = new String[]{"Obesidad","Desnutricion"};
			this.afeccion = afecciones[new Random().nextInt(afecciones.length)];
			
			//COMETARIO: PUEDE QUE UN SWITCH NO SEA TAN EFECTIVO ACA, PROBAR CON IF ANIDADOS
			switch (this.afeccion) {
			
			case ("Obesidad"):
				this.valor_afe = 0.1;
				break;
			
			case ("Desnutricion"):
				this.valor_afe = 0.1;
				break;
			}
		}
		
		else {
			this.afeccion = "Sin afeccion";
			this.valor_afe = 0.0;
		}
	}
	
	public String getAfeccion() {
		return this.afeccion;
	}
	
	//Nos devuelve el estado de la persona
	public String getEstado() {
		return this.estado;
	}
	
	//Formula para calcular la probabilidad de muerte
	public void formula() {
		
		if((this.inoculado) && (this.vac.getNombre().equals("Pfizer"))) {
			Double valor_temp = (this.valor_enf + this.valor_afe + (this.edad * 0.035) ) * this.vac.getEfectividad();
			this.formula = (this.valor_enf + this.valor_afe + (this.edad * 0.0035)) * valor_temp;
		}
		
		else if(!(this.inoculado)){
			this.formula = this.valor_enf + this.valor_afe + (this.edad * 0.0035);
		}
	
		int a = rd.nextInt(100)+1;
		
		//Aqui vemos si empeora su condicion
		if(this.formula > 0.35) {
			this.estado = "Grave";
		}
		
		//Aqui vemos si se muere o no
		if ((this.formula*100) >= a) {
			this.estado = "Muerto";
		}
	}
	
	//Funcion para que se recupere una persona despues de 5 dias
	public void contador() {
		this.cont++;
	}
	
	public int getCont() {
		return this.cont;
	}
}