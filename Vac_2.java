public class Vac_2 extends Vacuna{

	//ATRIBUTOS
	private int dosis;
	private Boolean inmunidad;
	
	//CONSTRUCTOR
	Vac_2(String nombre){
		super(nombre);
		this.dosis = 2;
		this.inmunidad = true;
	}
	
	//METODOS
	public Boolean getInmunidad() {
		return this.inmunidad;
	}
}
