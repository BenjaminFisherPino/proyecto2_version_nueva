public class Enfermedad {

	//ATRIBUTOS
	private Double prob_infeccion;
	
	//CONSTRUCTOR
	Enfermedad(Double prob){
		this.prob_infeccion = prob;
	}
	
	//METODOS
	public Double getProb_infeccion() {
		return this.prob_infeccion;
	}
}
