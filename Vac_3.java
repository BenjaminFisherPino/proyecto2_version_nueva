public class Vac_3 extends Vacuna{

	//ATRIBUTOS
	private Boolean inmunidad;
	
	//CONSTRUCTOR
	Vac_3(String nombre){
		super(nombre);
		this.inmunidad = true;
	}
	
	//METODOS
	public Boolean getInmunidad() {
		return this.inmunidad;
	}
}
