public class Vac_1 extends Vacuna{

	//ATRIBUTOS
	private int dosis;
	private Double efectividad;
	
	//CONSTRUCTOR
	Vac_1(String nombre){
		super(nombre);
		this.dosis = 2;
		this.efectividad = 0.25;
	}
		
	//METODOS
	public Double getEfectividad() {
		return this.efectividad;
	}
}
