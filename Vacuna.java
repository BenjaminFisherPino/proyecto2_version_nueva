public class Vacuna implements Interfaz_vacuna{

	//CLASE MADRE PARA LAS OTRAS VACUNAS
	
	//ATRIBUTOS EN COMUN
	private Boolean estado;
	private String nombre;
	
	
	//CONSTRUCTOR
	Vacuna(String nombre){
		this.estado = true;
		this.nombre = nombre;
	}
	
	//METODOS EN COMUN
	public Double getEfectividad() {
		return this.getEfectividad();
	}
	
	public void setEstado() {
		this.estado = false;
	}
	
	public Boolean getEstado() {
		return this.estado;
	}
	
	public String getNombre() {
		return this.nombre;
	}

}
