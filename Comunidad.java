import java.util.Collections;
import java.util.ArrayList;
import java.util.Random;

public class Comunidad {

	//ATRIBUTOS
	private int habitantes, enfermos_iniciales, contactos_estrechos;
	
	//CONTADORES DE VACUNAS Y ESTADOS DE PERSONA
	private int cont_1, cont_2, cont_3, cont_sanos, cont_enfermos, cont_graves, cont_muertos, cont_recuperados;
	private Double probabilidad_encuentro;
	private Persona persona;
	private Enfermedad enfermedad;
	private ArrayList<Persona> poblacion, infectados, lista_estrechos;
	private ArrayList<Vacuna> vacunas_1, vacunas_2, vacunas_3;
	private Vacuna vac;
	
	Random rd = new Random();
		
	//CONSTRUCTOR
	Comunidad(int habitantes, int enfermos_iniciales, int contactos_estrechos, Double probabilidad, Enfermedad a){
		this.habitantes = habitantes;
		this.enfermos_iniciales = enfermos_iniciales;
		this.contactos_estrechos = contactos_estrechos;
		this.probabilidad_encuentro = probabilidad;
		this.enfermedad = a;
		this.poblacion = new ArrayList<Persona>();
		this.vacunas_1 = new ArrayList<Vacuna>();
		this.vacunas_2 = new ArrayList<Vacuna>();
		this.vacunas_3 = new ArrayList<Vacuna>();
		this.infectados = new ArrayList<Persona>();
		this.lista_estrechos = new ArrayList<Persona>();
		this.cont_1 = 0;
		
		//Se crean persona
		for(int i = 0; i < this.habitantes ; i++) {
			int temp_edad = rd.nextInt(100)+1;
			persona = new Persona(i,temp_edad);
			persona.setAfeccion();
			persona.setEnfermedades();
			this.poblacion.add(persona);	
		}
		
		//Se crean vacunas en proporcion 25/16/9
		for(int i = 0; i < this.habitantes * 0.25 ;i++) {
			vac = new Vac_1("Pfizer");
			this.vacunas_1.add(vac);
		}
		
		for(int i = 0; i < this.habitantes * 0.16 ; i++) {
			vac = new Vac_2("Sinovac");
			this.vacunas_2.add(vac);
		}
		
		for(int i = 0; i< this.habitantes * 0.09 ; i++) {
			vac = new Vac_3("AstraZeneca");
			this.vacunas_3.add(vac);
		}
		
		//Infecta a las n primeras personas
		for(int i = 0; i < this.enfermos_iniciales;i++) {
			poblacion.get(i).setVirus(true);
			this.infectados.add(poblacion.get(i));
		}
		
		//Desordena la lista
		Collections.shuffle(this.poblacion);
	}
	
	//METODOS	
	public void vacunar() {
		for (int i = 0; i < this.habitantes; i++) {
			
			//SI LA PERSONA NO ESTA MUERTA O RECUPERADA
			if(!(poblacion.get(i).getEstado().equals("Muerto")) && !(poblacion.get(i).getEstado().equals("Recuperado"))) {
				
				//SE ESCOJE UNA VACUNA
				int temp_vacuna = rd.nextInt(3)+1;
				
				//CASO VACUNA 1
				if((temp_vacuna == 1) && (this.cont_1 < this.vacunas_1.size())) {	
					poblacion.get(i).setVacuna(this.vacunas_1.get(this.cont_1));
					this.vacunas_1.get(this.cont_1).setEstado();
					this.cont_1++;
					}
				
				
				else if((temp_vacuna == 2) && (this.cont_2 < this.vacunas_2.size())){
					poblacion.get(i).setVacuna(this.vacunas_2.get(this.cont_2));
					this.vacunas_2.get(this.cont_2).setEstado();
					this.cont_2++;
				}
				
				else if((temp_vacuna == 3) && (this.cont_3 < this.vacunas_3.size())){
					poblacion.get(i).setVacuna(this.vacunas_3.get(this.cont_3));
					this.vacunas_3.get(this.cont_3).setEstado();
					this.cont_3++;
				}
			}
		}
	}
	
	//private
    public void infectar(){

        // Por cada persona de la lista infectados, se obtiene un grupo pequeño
        // de contactos estrechos.
        for(Persona p: this.infectados){
            Collections.shuffle(this.poblacion);
            for(int i=0; i<this.contactos_estrechos; i++){
                this.lista_estrechos.add(this.poblacion.get(i));
            } 

            comprobarInfeccion(p, this.lista_estrechos);
            this.lista_estrechos.clear();
        }
    }

    public void comprobarInfeccion(Persona infectado, ArrayList<Persona> estrechos){
        
        for (Persona p: estrechos){
            //Si la persona no esta ya infectada ni se encuentra recuperada es susceptible
            if(p.getEstado().equals("Sano")){
                //Se comprueba que exista un contacto fisico segun aleatoriedad
                if(this.rd.nextDouble() < this.contactos_estrechos){
                    //Se comprueba si realmente se pego la enfermedad
                    if(this.rd.nextDouble() < this.enfermedad.getProb_infeccion()){
                    	p.setVirus(true);
                    }
                }
            }
        }	
    }
	
	public void recuperar() {
		for(Persona p: this.infectados) {
			if(p.getCont() == 5) {
				p.setVirus(false);
			}
		}
	}
	
	public void verificador() {
		
		for(Persona p: this.infectados) {			
			if(p.getInoculado()) {
				
				p.contador();
				
				if(p.getVacuna().getNombre().equals("Pfizer")) {
					p.formula();
				}

				//Si se pone la vacuna 2 no puede estar grave
				if(p.getVacuna().getNombre().equals("Sinovac") && (p.getEstado().equals("Grave"))) {
					p.setEstado("Enfermo");
				}
				
				//Si se pone la vacuna 3 queda inmune y recuperado
				else if(p.getVacuna().getNombre().equals("AstraZeneca") && ((p.getEstado().equals("Grave")) || (p.getEstado().equals("Enfermo")))) {
					p.setEstado("Recuperado");
				}
			}	
			
			else {
				p.contador();
				p.formula();
			}
		}				
	}
	
	//ESTA FUNCION NOS PERMITE CONTAR EN CADA STEP
	//CUANTOS SANOS,ENFERMOS,GRAVES,MUERTOS Y RECUPERADOS
	//HAY, ADEMAS DE LAS VACUNAS DEMAS DATOS PARA EL PRINT
	public void cuentaPersonas() {
		
		for(Persona p: this.poblacion) {
			//NO ESTOY SEGURO QUE ESTO VAYA A FUNCIONAR
			switch(p.getEstado()) {
			
			case ("Sano"):
				this.cont_sanos++;
				break;
			
			case ("Enfermo"):
				this.cont_enfermos++;
				break;
			
			case ("Grave"):
				this.cont_graves++;
				break;
			
			case ("Muerto"):
				this.cont_muertos++;
				break;
				
			case ("Recuperado"):
				this.cont_recuperados++;
				break;
			}
		}
		
		System.out.println("SANOS: " + this.cont_sanos);
		System.out.println("ENFERMOS: " + this.cont_enfermos);
		System.out.println("GRAVES: " + this.cont_graves);
		System.out.println("MUERTOS: " + this.cont_muertos);
		System.out.println("RECUPERADOS: " + this.cont_recuperados + "\n");
//		this.cont_sanos = 0;
//		this.cont_enfermos = 0;
//		this.cont_graves = 0;
//		this.cont_muertos = 0;
//		this.cont_recuperados = 0;
	}
		
	//ESTA FUNCION SIRVE PARA PROBAR DATOS DURANTE EL DESARROLLO
//	public void imprime(){
//		int temp_cont = 0;
//		for(Persona r: this.poblacion) {
//			System.out.println(temp_cont + ") Afeccion: " + r.getAfeccion());
//			System.out.println("   Enfermedad: " + r.getEnfermedad());
//			System.out.println("Virus: " + r.getVirus());
//			System.out.println(r.getEstado());
//			temp_cont++;
//		}
//	}
}
